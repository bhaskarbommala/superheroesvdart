package bbommala.com.superheros.dependency.modules.appmodules

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import bbommala.com.superheros.dependency.SuperHerosService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val REQUEST_TIMEOUT = 10
private const val BASE_URL =
    "https://bitbucket.org/dttden/mobile-coding-challenge/"

@Module
class NetworkModule {


    @Singleton
    @Provides
    fun providesClient(application: Application): OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val superHeroesCache = Cache(application.cacheDir, cacheSize)

        return OkHttpClient.Builder()
            .cache(superHeroesCache)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(application)!!)
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 2
                    ).build()
                chain.proceed(request)
            }.build()


    }


    @Singleton
    @Provides
    fun provideRetrofit(application: Application): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(providesClient(application))
            .build()
    }

    @Singleton
    @Provides
    fun provideSuperHerosService(retrofit: Retrofit): SuperHerosService {
        return retrofit.create(SuperHerosService::class.java)
    }

    private fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }

}