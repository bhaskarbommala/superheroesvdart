package bbommala.com.superheros.dependency.modules.appmodules

import bbommala.com.superheros.views.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidViewModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}