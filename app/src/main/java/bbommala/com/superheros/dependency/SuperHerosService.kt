package bbommala.com.superheros.dependency

import bbommala.com.superheros.models.SuperHeros
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface SuperHerosService {
    @GET("raw/2ee8bd47703c62c5d217d9fb9e0306922a34e581/{year}")
    fun fetchSuperheroes(@Path(value = "year") year: String): Single<SuperHeros>
}