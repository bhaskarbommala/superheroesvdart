package bbommala.com.superheros.dependency

import android.app.Application
import bbommala.com.profiles.dependency.AppModule
import bbommala.com.superheros.dependency.modules.appmodules.AndroidViewModule
import bbommala.com.superheros.dependency.modules.appmodules.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, AppModule::class, AndroidInjectionModule::class, AndroidViewModule::class])
interface SuperHerosComponent : AndroidInjector<SuperHerosApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): SuperHerosComponent
    }
}