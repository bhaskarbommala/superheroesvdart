package bbommala.com.superheros.dependency

import bbommala.com.superheros.models.SuperHeros
import io.reactivex.Single
import javax.inject.Inject

class SuperHerosProvider @Inject constructor(private val superHerosService: SuperHerosService) {
    fun fetchSuperheroes(year: String): Single<SuperHeros> = superHerosService.fetchSuperheroes(year)
}