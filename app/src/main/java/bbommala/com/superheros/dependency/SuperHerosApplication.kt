package bbommala.com.superheros.dependency

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class SuperHerosApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerSuperHerosComponent
            .builder()
            .application(this)
            .build()
    }

}