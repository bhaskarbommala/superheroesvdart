package bbommala.com.superheros.viewholders

import android.support.v7.widget.RecyclerView
import bbommala.com.superheros.viewmodels.SuperHeroItemViewModel

class SuperHerosViewHolder(var binding: bbommala.com.superheros.databinding.ItemSuperHeroBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(superHeroItemViewModel: SuperHeroItemViewModel){
        binding.itemViewModel = superHeroItemViewModel
        binding.executePendingBindings()
    }
}