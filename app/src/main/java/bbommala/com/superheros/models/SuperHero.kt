package bbommala.com.superheros.models

data class SuperHero(
    val Name: String,
    val Picture: String,
    val Score: Int
)