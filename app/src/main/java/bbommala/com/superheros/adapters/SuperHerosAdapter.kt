package bbommala.com.superheros.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import bbommala.com.superheros.R
import bbommala.com.superheros.databinding.ItemSuperHeroBinding
import bbommala.com.superheros.viewholders.SuperHerosViewHolder
import bbommala.com.superheros.viewmodels.SuperHeroItemViewModel
import javax.inject.Inject

class SuperHerosAdapter @Inject constructor() : RecyclerView.Adapter<SuperHerosViewHolder>() {

    private val viewModlesList = mutableListOf<SuperHeroItemViewModel>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): SuperHerosViewHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val binding = DataBindingUtil.inflate<ItemSuperHeroBinding>(
            layoutInflater,
            R.layout.item_super_hero,
            viewGroup,
            false
        )
        return SuperHerosViewHolder(binding)
    }

    override fun getItemCount(): Int = viewModlesList.size

    override fun onBindViewHolder(holder: SuperHerosViewHolder, positin: Int) {
        holder.bind(viewModlesList[positin])
    }

    fun setViewModels(list: List<SuperHeroItemViewModel>) {
        this.viewModlesList.clear()
        this.viewModlesList.addAll(list)
        this.notifyDataSetChanged()

    }
}