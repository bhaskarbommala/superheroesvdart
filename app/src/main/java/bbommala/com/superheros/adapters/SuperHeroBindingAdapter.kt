package bbommala.com.superheros.adapters

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

object SuperHeroBindingAdapter {

    @BindingAdapter("layoutManager")
    @JvmStatic
    fun setLayoutManager(view: RecyclerView, orientation: Int) {
        view.layoutManager = LinearLayoutManager(view.context, orientation, false)
    }


    @BindingAdapter("setImageSource")
    @JvmStatic
    fun setImageSource(view: ImageView, rawImageUrl: String) {
        val imageUrl: String = rawImageUrl.replace(".jpeg", ".jpg", true)
        val imageViewUrl =
            "https://bitbucket.org/dttden/mobile-coding-challenge/raw/2ee8bd47703c62c5d217d9fb9e0306922a34e581/$imageUrl"
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(view.context)
            .load(imageViewUrl)
            .apply(requestOptions)
            .into(view)

    }

    @BindingAdapter("setRefresh")
    @JvmStatic
    fun setRefreshing(view: SwipeRefreshLayout, shouldSetRefresh: Boolean) {
        view.isRefreshing = shouldSetRefresh

    }
}