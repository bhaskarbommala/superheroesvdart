package bbommala.com.superheros.views

import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import bbommala.com.superheros.R
import bbommala.com.superheros.databinding.ActivityMainBinding
import bbommala.com.superheros.viewmodels.MainViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModel: MainViewModel

    lateinit var animator: ObjectAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        val dataBinding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main, null
        )
        dataBinding.mainViewModel = mainViewModel

        animator = AnimatorInflater.loadAnimator(
            this@MainActivity,
            R.animator.animation_left_to_right
        ) as ObjectAnimator
        animator.target = swipeContainer
        mainViewModel.setAnimator(animator)
        dataBinding.swipeContainer.setOnRefreshListener {
            mainViewModel.onRefresh()
        }


    }
}
