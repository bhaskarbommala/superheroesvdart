package bbommala.com.superheros.viewmodels

import android.animation.ObjectAnimator
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.util.Log
import android.widget.Toast
import bbommala.com.superheros.adapters.SuperHerosAdapter
import bbommala.com.superheros.dependency.SuperHerosProvider
import bbommala.com.superheros.models.SuperHeros
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val networkRepository: SuperHerosProvider,
    val superHerosAdapter: SuperHerosAdapter
) {
    private val superHerosList = mutableListOf<SuperHeroItemViewModel>()
    private val superHerosYears = mutableListOf(2006, 2009, 2012, 2015, 2018)
    private var currentIndexOfYears: Int = 0
    val yearOfSuperHeros = ObservableField<String>(superHerosYears[currentIndexOfYears].toString())
    val shouldRefersh = ObservableBoolean(false)
    private lateinit var animator: ObjectAnimator

    init {
        fetchSuperHeroes()
    }

    fun onRefresh() {
        shouldRefersh.set(true)
        refreshSuperHerosOnPull()
    }

    fun setAnimator(animator: ObjectAnimator) {
        this.animator = animator
    }

    private fun fetchSuperHeroes() {
        networkRepository.fetchSuperheroes("${this.superHerosYears[currentIndexOfYears]}.json")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showData, this::showError)
    }

    private fun refreshSuperHerosOnPull() {
        superHerosList.clear()
        superHerosAdapter.notifyDataSetChanged()
        if (currentIndexOfYears < superHerosYears.size - 1) {
            currentIndexOfYears++
        } else {
            currentIndexOfYears = 0
        }
        fetchSuperHeroes()
    }

    private fun showData(superHeros: SuperHeros) {
        Log.d("MainActivity", "Values from Server is$superHeros")
        val sortedSuperHeros = superHeros.Heroes.sortedWith(compareByDescending { it.Score })
        yearOfSuperHeros.set(superHerosYears[currentIndexOfYears].toString())
        shouldRefersh.set(false)
        animator.start()
        for (superHero in sortedSuperHeros) {
            superHerosList.add(
                SuperHeroItemViewModel(
                    superHero.Name,
                    superHero.Picture,
                    superHero.Score.toString()
                )
            )
        }
        superHerosAdapter.setViewModels(superHerosList)
        superHerosAdapter.notifyDataSetChanged()
    }

    private fun showError(throwable: Throwable) {
        //Handle error here
    }

}